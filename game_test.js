var canvas = document.getElementById('myCanvas')
// shape functions
// triangle
function drawTriangle () {
  var ctx = canvas.getContext('2d')
  if (canvas.getContext) {
    ctx.fillStlye = '#7FFFD4'
    ctx.beginPath()
    ctx.moveTo(440, 420)
    ctx.lineTo(280, 420)
    ctx.lineTo(360, 300)
    ctx.fill()
  }
}
// square
function drawSquare () {
  var ctx = canvas.getContext('2d')
  if (canvas.getContext) {
    ctx.fillStyle = '#F5F5DC'
    ctx.fillRect(24, 24, 120, 120)
    ctx.clearRect(48, 48, 72, 72)
    ctx.strokeRect(35, 35, 98, 98)
    ctx.strokeRect(48, 48, 72, 72)
  }
}
// circle
function drawCircle () {
  var ctx = canvas.getContext('2d')
  if (canvas.getContext) {
    ctx.beginPath()
    ctx.arc(340, 80, 50, 0, Math.PI * 2)
    ctx.fillStyle = '#7FFFD4'
    ctx.fill()
    ctx.closePath()
  }
}

// draw left canvas
function drawLeft () {
  var canvas = document.getElementById('canvasL')
  if (canvas.getContext) {
    drawTriangle()
    drawCircle()
    drawSquare()
  }
}
// draw right canvas
function drawRight () {
  var canvas = document.getElementById('canvasR')
  if (canvas.getContext) {
    drawTriangle()
    drawCircle()
    drawSquare()
  }
}

function draw () {
  // canvas.addEventListener('click', function (event) {
  //   var tri = canvas.getBoundingClientRect()
  //   var x = event.clientX - tri.left
  //   var y = event.clientY - tri.top
  //   console.log('left x: ' + x + ' left y: ' + y)
  // }, false)

  drawLeft(
    addEvent(
      document.getElementById('canvasL'),
      'click',
      function () {
        alert('you clicked the left canvas')
      }
    )
  )
  drawRight(
    addEvent(
      document.getElementById('canvasR'),
      'click',
      function () {
        alert('you clicked the right canvas')
      }
    )
  )
}

// a beginning for shape detection
function addEvent (element, evnt, funct) {
  if (element.attachEvent) {
    return element.attachEvent('on' + evnt, funct)
  } else {
    return element.addEventListener(evnt, funct, false)
  }
}

draw()
// collides()
